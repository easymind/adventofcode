package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func day2data() []int {
	dat, _ := ioutil.ReadFile("input/day2.txt")
	var list []int
	for _, v := range strings.Split(string(dat), ",") {
		if i, err := strconv.Atoi(v); err == nil {
			list = append(list, i)
		}
	}
	list[1] = 12
	list[2] = 2
	return list
}
func day2a() {
	start := start()
	var list = day2data()
	for index := 0; index < len(list)-4 && list[index] != 99; index += 4 {
		val1 := list[list[index+1]]
		val2 := list[list[index+2]]
		if list[index] == 1 {
			list[list[index+3]] = val1 + val2
		} else if list[index] == 2 {
			list[list[index+3]] = val1 * val2
		}
	}
	fmt.Println(list[0])
	fmt.Println("time: ", timing(start))
}

func day2b() {
	start := start()
	var startlist = day2data()
	var answer = 0
	var i1 = -1
	var i2 = 0
	for answer != 19690720 && i2 < 100 {
		list := append(startlist[:0:0], startlist...)
		i1++
		if i1 > 99 {
			i1 = 0
			i2++
		}
		list[1] = i1
		list[2] = i2
		for index := 0; index < len(list)-4 && list[index] != 99; index += 4 {
			val1 := list[list[index+1]]
			val2 := list[list[index+2]]
			if list[index] == 1 {
				list[list[index+3]] = val1 + val2
			} else if list[index] == 2 {
				list[list[index+3]] = val1 * val2
			}
		}
		answer = list[0]
	}

	fmt.Printf("%v%v\n", i1, i2)
	fmt.Println("time: ", timing(start))
}
