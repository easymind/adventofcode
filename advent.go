package main

import (
	"time"
)

func main() {
	// day1a()
	// day1b()
	// day2a()
	// day2b()
	day3a()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func abs(n int) int {
	if n < 0 {
		n *= -1
	}
	return n
}
func start() int64 {
	return time.Now().UnixNano()
}

func timing(starttime int64) float32 {
	return float32(start()-starttime) / 1000000000
}
