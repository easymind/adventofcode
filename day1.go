package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func day1a() {
	start := start()
	dat, _ := ioutil.ReadFile("input/day1.txt")
	listOfStrings := strings.Split(string(dat), "\r\n")
	var totalFuel int
	for _, strVal := range listOfStrings {
		mass, _ := strconv.Atoi(strVal)
		totalFuel += ((mass / 3) - 2)
	}
	fmt.Println(totalFuel)
	fmt.Println("time: ", timing(start))
}

func day1b() {
	start := start()
	dat, _ := ioutil.ReadFile("input/day1.txt")
	listOfStrings := strings.Split(string(dat), "\r\n")
	var totalFuel int
	for _, strVal := range listOfStrings {
		mass, _ := strconv.Atoi(strVal)
		for extraMass := ((mass / 3) - 2); extraMass > 0; extraMass = ((extraMass / 3) - 2) {
			totalFuel += extraMass
		}
	}
	fmt.Println(totalFuel)
	fmt.Println("time: ", timing(start))
}
