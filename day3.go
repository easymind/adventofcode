package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

type wirepath struct {
	direction string
	distance  int
}
type coordinate struct {
	x int
	y int
}
type line struct {
	begin   int
	end     int
	axis    string
	axispos int
}
type compareLine struct {
	l1 line
	l2 line
}

func day3data() [][]wirepath {
	dat, _ := ioutil.ReadFile("input/day3.txt")
	datwires := strings.Split(string(dat), "\n")
	var wires = [][]wirepath{[]wirepath{}, []wirepath{}}
	for index, line := range datwires {
		values := strings.Split(line, ",")
		for _, v := range values {
			direction := v[:1]
			if distance, err := strconv.Atoi(v[1:]); err == nil {
				wires[index] = append(wires[index], wirepath{direction, distance})
			}
		}
	}
	return wires
}

func day3a() {
	start := time.Now()
	wires := day3data()
	fmt.Printf("Read file: %v\n", time.Since(start))

	time.Sleep(1000)
	start = time.Now()
	var hitlines = [][]line{[]line{}, []line{}}
	for index, wire := range wires {
		pos := coordinate{0, 0}
		for _, v := range wire {
			var begin int
			var end int
			var target string
			switch v.direction {
			case "U":
				begin = pos.y
				end = pos.y + v.distance
				target = "y"
				pos.y = end
			case "R":
				begin = pos.x
				end = pos.x + v.distance
				target = "x"
				pos.x = end
			case "D":
				begin = pos.y - v.distance
				end = pos.y
				target = "y"
				pos.y = begin
			case "L":
				begin = pos.x - v.distance
				end = pos.x
				target = "x"
				pos.x = begin
			}
			switch target {
			case "x":
				hitlines[index] = append(hitlines[index], line{begin, end, target, pos.y})
			case "y":
				hitlines[index] = append(hitlines[index], line{begin, end, target, pos.x})
			}
		}
	}
	fmt.Printf("Convert to line data: %v\n", time.Since(start))

	time.Sleep(1000)
	start = time.Now()
	var startedRoutines = 0
	var closestdistance = 0
	var channelItems = make(chan compareLine, 100)
	go compareLines(channelItems, &closestdistance)
	for _, l1 := range hitlines[0] {
		for _, l2 := range hitlines[1] {
			channelItems <- compareLine{l1, l2}
		}
	}
	fmt.Printf("Found closest with %v channels %v: %v\n", startedRoutines, closestdistance, time.Since(start))

	// time.Sleep(1000)
	// start = time.Now()
	// closestdistance = 0
	// compareLines(clis, &closestdistance)
	// fmt.Printf("Found closest without channels %v: %v\n", closestdistance, time.Since(start))

	// time.Sleep(1000)
	// start = time.Now()
	// var half = len(clis) / 2
	// var closestdistanceChan = 0
	// go compareLines(clis[:half], &closestdistanceChan)
	// compareLines(clis[half:], &closestdistanceChan)
	// fmt.Printf("Found closest with channels %v: %v\n", closestdistanceChan, time.Since(start))
}

func compareLines(lis <-chan compareLine, output *int) {
	for li := range lis {
		if li.l1.axis != li.l2.axis && li.l2.axispos >= li.l1.begin && li.l2.axispos <= li.l1.end && li.l1.axispos >= li.l2.begin && li.l1.axispos <= li.l2.end {
			var hitsum = abs(li.l1.axispos) + abs(li.l2.axispos)
			if *output == 0 || hitsum < *output {
				*output = hitsum
			}
		}
	}
	time.Sleep(1)
}
func day3b() {

}
